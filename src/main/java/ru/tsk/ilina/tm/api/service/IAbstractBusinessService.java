package ru.tsk.ilina.tm.api.service;

import ru.tsk.ilina.tm.enumerated.Role;
import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.exception.empty.EmptyNameException;
import ru.tsk.ilina.tm.exception.empty.EmptyUserIdException;
import ru.tsk.ilina.tm.model.AbstractBusinessEntity;
import ru.tsk.ilina.tm.model.Project;
import ru.tsk.ilina.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface IAbstractBusinessService<E extends AbstractBusinessEntity> extends IAbstractOwnerService<E> {

    E removeByName(String userId, String name);

    E findByName(String userId, String name);

    E updateById(String userId, String id, String name, String description);

    E changeStatusByName(String userId, String name, Status status);

    E changeStatusByID(String userId, String id, Status status);

    E finishByName(final String userId, String name);

    E removeByIndex(String userId, Integer index);

    E findByIndex(final String userId, final Integer index);

    E updateByIndex(final String userId, final Integer index, final String name, final String description);

    E startByIndex(final String userId, final Integer index);

    E finishByIndex(final String userId, final Integer index);

    E changeStatusByIndex(final String userId, final Integer index, final Status status);

    E startByID(String userId, String id);

    E startByName(String userId, String name);

    E finishByID(String userId, String id);

    void create(final String userId, String name, String description);

}
