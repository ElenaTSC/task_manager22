package ru.tsk.ilina.tm.api.service;

import ru.tsk.ilina.tm.api.repository.IAbstractRepository;
import ru.tsk.ilina.tm.enumerated.Role;
import ru.tsk.ilina.tm.model.User;

public interface IUserService extends IAbstractService<User> {

    User addUser(String login, String password);

    User addUser(String login, String password, String email);

    User addUser(String login, String password, Role role);

    User findByEmail(String email);

    User setPassword(String userId, String password);

    User updateUser(String userId, String firstName, String lastName, String middleName);

    User removeByLogin(String login);

    User findByLogin(String login);

    boolean isLoginExists(String login);

    User lockUserByLogin(String login);

    User unlockUserByLogin(String login);

}
