package ru.tsk.ilina.tm.command.user;

import ru.tsk.ilina.tm.command.AbstractAuthUserCommand;
import ru.tsk.ilina.tm.enumerated.Role;
import ru.tsk.ilina.tm.util.TerminalUtil;

public class UserByLoginLockCommand extends AbstractAuthUserCommand {

    @Override
    public String name() {
        return "user-lock-by-login";
    }

    @Override
    public String description() {
        return "Lock user by login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("[ENTER LOGIN: ");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().lockUserByLogin(login);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }
}
