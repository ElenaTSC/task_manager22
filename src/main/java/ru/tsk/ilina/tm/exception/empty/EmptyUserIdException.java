package ru.tsk.ilina.tm.exception.empty;

import ru.tsk.ilina.tm.exception.AbstractException;

public class EmptyUserIdException extends AbstractException {

    public EmptyUserIdException() {
        super("Error! UserId is empty");
    }

    public EmptyUserIdException(String message) {
        super("Error! " + message + " userid is empty");
    }

}
