package ru.tsk.ilina.tm.repository;

import ru.tsk.ilina.tm.api.repository.ITaskRepository;
import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.model.Project;
import ru.tsk.ilina.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    @Override
    public Task bindTaskToProjectById(final String userId, String projectId, String taskId) {
        final Task task = findByID(userId, taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public List<Task> findAllTaskByProjectId(final String userId, String projectId) {
        return entities.stream().filter(o -> userId.equals(o.getUserId()) && projectId.equals(o.getProjectId())).collect(Collectors.toList());
    }

    @Override
    public Task unbindTaskToProjectById(final String userId, String projectId, String taskId) {
        final Task task = findByID(userId, taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

    @Override
    public void removeAllTaskByProjectId(final String userId, String projectId) {
        findAllTaskByProjectId(userId, projectId).forEach(o -> entities.remove(o.getId()));
    }

}
