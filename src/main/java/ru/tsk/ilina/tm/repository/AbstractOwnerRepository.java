package ru.tsk.ilina.tm.repository;

import ru.tsk.ilina.tm.api.repository.IAbstractBusinessRepository;
import ru.tsk.ilina.tm.api.repository.IAbstractOwnerRepository;
import ru.tsk.ilina.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E> implements IAbstractOwnerRepository<E> {

    public static Predicate<AbstractOwnerEntity> predicateById(final String userId) {
        return o -> userId.equals(o.getUserId());
    }

    @Override
    public void remove(final String userId, final E entity) {
        entities.stream().filter(predicateById(userId)).collect(Collectors.toList()).remove(entity);
    }

    @Override
    public void clear(final String userId) {
        findAll(userId).forEach(o -> entities.remove(o.getId()));
    }

    @Override
    public Integer getSize(String userId) {
        return entities.stream().filter(predicateById(userId)).collect(Collectors.toList()).size();
    }

    @Override
    public void add(final String userId, final E entity) {
        entity.setUserId(userId);
        entities.add(entity);
    }

    @Override
    public List<E> findAll(final String userId) {
        return entities.stream().filter(predicateById(userId)).collect(Collectors.toList());
    }

    @Override
    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        return entities.stream().filter(predicateById(userId)).sorted(comparator).collect(Collectors.toList());
    }

    @Override
    public E findByIndex(final String userId, final Integer index) {
        final List<E> entity = findAll(userId);
        return entity.get(index);
    }

    @Override
    public boolean existsById(String userId, String id) {
        return Optional.ofNullable(findByID(userId, id)).isPresent();
    }

    @Override
    public E removeByIndex(final String userId, final Integer index) {
        final Optional<E> entity = Optional.ofNullable(findByIndex(userId, index));
        if (entity.isPresent())
            entities.remove(entity.get());
        return entity.get();
    }

    @Override
    public E findByID(final String userId, final String id) {
        return entities.stream().filter(o -> userId.equals(o.getUserId()) && id.equals(o.getId())).findFirst().orElse(null);
    }

    @Override
    public E removeByID(final String userId, final String id) {
        final Optional<E> entity = Optional.ofNullable(findByID(userId, id));
        if (entity.isPresent())
            entities.remove(entity.get());
        return entity.get();
    }

}
