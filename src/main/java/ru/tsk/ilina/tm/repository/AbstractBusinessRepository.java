package ru.tsk.ilina.tm.repository;

import ru.tsk.ilina.tm.api.repository.IAbstractBusinessRepository;
import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.model.*;

import java.util.Optional;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity> extends AbstractOwnerRepository<E> implements IAbstractBusinessRepository<E> {

    public Optional<E> updateStatus(final E entity, final Status status) {
        Optional<E> optional = Optional.ofNullable(entity);
        optional.ifPresent(e -> e.setStatus(status));
        return optional;
    }

    @Override
    public E removeByName(final String userId, final String name) {
        final E entity = findByName(userId, name);
        Optional.ofNullable(entity).ifPresent(this::remove);
        return entity;
    }

    @Override
    public E findByName(final String userId, final String name) {
        return entities.stream().filter(o -> userId.equals(o.getUserId()) && name.equals(o.getName())).findFirst().orElse(null);
    }

    @Override
    public E startByID(final String userId, String id) {
        final E entity = findByID(userId, id);
        return updateStatus(entity, Status.IN_PROGRESS).orElse(null);
    }

    @Override
    public E startByIndex(final String userId, Integer index) {
        final E entity = findByIndex(userId, index);
        return updateStatus(entity, Status.IN_PROGRESS).orElse(null);
    }

    @Override
    public E startByName(final String userId, String name) {
        final E entity = findByName(userId, name);
        return updateStatus(entity, Status.IN_PROGRESS).orElse(null);
    }

    @Override
    public E changeStatusByID(final String userId, String id, Status status) {
        final E entity = findByID(userId, id);
        return updateStatus(entity, status).orElse(null);
    }

    @Override
    public E changeStatusByIndex(final String userId, Integer index, Status status) {
        final E entity = findByIndex(userId, index);
        return updateStatus(entity, status).orElse(null);
    }

    @Override
    public E changeStatusByName(final String userId, String name, Status status) {
        final E entity = findByName(userId, name);
        return updateStatus(entity, status).orElse(null);
    }

    @Override
    public E finishByID(final String userId, String id) {
        final E entity = findByID(userId, id);
        return updateStatus(entity, Status.COMPLETED).orElse(null);
    }

    @Override
    public E finishByIndex(final String userId, Integer index) {
        final E entity = findByIndex(userId, index);
        return updateStatus(entity, Status.COMPLETED).orElse(null);
    }

    @Override
    public E finishByName(final String userId, String name) {
        final E entity = findByName(userId, name);
        return updateStatus(entity, Status.COMPLETED).orElse(null);
    }

}
