package ru.tsk.ilina.tm.service;

import ru.tsk.ilina.tm.api.service.IAuthService;
import ru.tsk.ilina.tm.api.service.IUserService;
import ru.tsk.ilina.tm.enumerated.Role;
import ru.tsk.ilina.tm.exception.user.AccessDeniedException;
import ru.tsk.ilina.tm.exception.empty.EmptyIdException;
import ru.tsk.ilina.tm.exception.empty.EmptyLoginException;
import ru.tsk.ilina.tm.exception.empty.EmptyPasswordException;
import ru.tsk.ilina.tm.model.User;
import ru.tsk.ilina.tm.util.HashUtil;

public final class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        if (user.getLocked()) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void registry(final String login, final String password, final String email) {
        userService.addUser(login, password, email);
    }

    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public User getUser() {
        final String userId = getUserId();
        return userService.findById(userId);
    }

    @Override
    public void updateUser(final String userId, final String firstName, final String lastName, final String middleName) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        userService.updateUser(userId, firstName, lastName, middleName);
    }

    @Override
    public void setPassword(final String userId, final String password) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        userService.setPassword(userId, password);
    }

    public boolean isAuth() {
        return userId == null;
    }

    public void checkRoles(final Role... roles) {
        if (roles == null || roles.length == 0) return;
        final User user = getUser();
        if (user == null) throw new AccessDeniedException();
        final Role role = user.getRole();
        if (role == null) throw new AccessDeniedException();
        for (final Role item : roles) {
            if (item.equals(role)) return;
        }
        throw new AccessDeniedException();
    }

}
